package rrmq

import (
	"bytes"
	"fmt"
	"strconv"
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

func Test_TwoCopy(t *testing.T) {
	begin(3)
	// 	strCfgWOConsumer := `{
	//   "server": {
	//     "addr": "localhost",
	//     "port": 5672,
	//     "login": "guest",
	//     "password": "guest"
	//   },
	//   "queues": {
	//     "q2207": {
	//       "consume": false,
	//       "durable": true,
	//       "exclusive": false,
	//       "autoDelete": false,
	//       "name": "q2207",
	//       "qos": 10
	//     }
	//   }
	// }`
	
	strCfgWConsumer := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207",
      "qos": 10
    }
  }
}`
	
	logPrefix := ee.NewPrefix()
	
	for i := 0; i < 5; i++ {
		lp := ee.AddPrefix(logPrefix, " #"+strconv.Itoa(i))
		fmt.Printf("Test #%d\n", i)
		cfg, err := ParseConfig("logPrefix", []byte(strCfgWConsumer))
		if err != nil {
			t.Error(err)
			return
		}
		
		rmq1, err := GetRMQ(lp, cfg)
		
		ch, _ := rmq1.GetConsumerChan(logPrefix, "q2207")
		
		body := []byte("test string::" + time.Now().Format(time.RFC3339))
		
		rmq1.ThoroughlySend(lp, "q2207", body, nil, time.Second)
		
		msg := <-ch
		fmt.Printf("\t!%s!\n\t!%s!\n", string(msg.Body), string(body))
		if bytes.Compare(msg.Body, body) != 0 {
			t.Error("Ошибка", string(msg.Body), "<===>", string(body))
		}
		msg.Ack(false)
		
		// time.Sleep(time.Second)
		rmq1.Done(lp)
		
	}
	time.Sleep(time.Second)
	
	// rmq2, err := GetRMQ(ee.NewPrefix(), cfg)
	//
	// rmq2.Done(ee.NewPrefix())
	// // time.Sleep(time.Second)
	//
	// time.Sleep(60 * time.Second)
	
}

func GetRMQ(logPrefix ee.LogPrefixType, cfg Config) (*Obj, error) {
	
	smallRMQ, err := Init(logPrefix, cfg)
	if err != nil {
		return smallRMQ, err
	}
	
	go func(smallRMQ *Obj) {
		for {
			select {
			case s := <-smallRMQ.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в smallRMQ:" + s))
			case <-smallRMQ.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с smallRMQ"))
				return
				// 	case <-doneChan:
				// 		ll.Log(ee.Debug().WithMessage("Получена команда на завершение работа модуля: отдаем команду smallRMQ заканчивать работу"))
				// 		smallRMQ.Done(logPrefix)
				// 		return
			}
		}
	}(smallRMQ)
	
	return smallRMQ, nil
}
