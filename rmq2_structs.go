package rrmq

// ConnectConf - настройки коннекта
type ConnectConf struct {
	Login    string `json:"login"`
	Password string `json:"password"`
	Addr     string `json:"addr"`
	Port     int    `json:"port"`
}

type CfgRmqExchange struct {
	Name       string                 `json:"name"`
	Type       string                 `json:"type"`
	Durable    bool                   `json:"durable"`
	AutoDelete bool                   `json:"autoDelete"`
	Args       map[string]interface{} `json:"args"`
}

// CfgRmq настройки RabbitMQ
type CfgRmq struct {
	Title      string         `json:"title"`
	Connect    ConnectConf    `json:"connectConf"`
	Queue      CfgRmqQueue    `json:"queue"`
	Exchange   CfgRmqExchange `json:"exchange"`
	RoutingKey string         `json:"routingKey"`
	QOS        int            `json:"qos"`
}

// CfgRmqQueue настройки RabbitMQ очереди
type CfgRmqQueue struct {
	Name       string `json:"name"`
	Durable    bool   `json:"durable"`
	Exclusive  bool   `json:"exclusive"`
	AutoDelete bool   `json:"autoDelete"`
}

type CfgBinding struct {
	Queue    string `json:"queue"`
	Exchange string `json:"exchange"`
}

type ConfigQueue struct {
	Name       string                 `json:"name"`
	Consume    bool                   `json:"consume"`
	AutoAck    bool                   `json:"autoAck"`
	Durable    bool                   `json:"durable"`
	Exclusive  bool                   `json:"exclusive"`
	AutoDelete bool                   `json:"autoDelete"`
	QOS        int                    `json:"qos"`
	Args       map[string]interface{} `json:"args"`
}

type Config struct {
	Server    ConnectConf               `json:"server"`
	Queues    map[string]ConfigQueue    `json:"queues"`
	Exchanges map[string]CfgRmqExchange `json:"exchanges"`
	Bindings  []CfgBinding              `json:"bindings"`
}

type TagT struct {
	App   string `json:"app"`
	Pid   int    `json:"pid"`
	Host  string `json:"host"`
	Since string `json:"since"`
}
