package rrmq

import (
	"fmt"
	"strconv"
	"sync"
	"testing"
	"time"
	
	"github.com/streadway/amqp"
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

func Test_ThoroughlySend(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207",
      "qos": 1
    }
  }
}`
	
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	rmq.ThoroughlySend(ee.NewPrefix(), "q2207", []byte("какой-то текст"), nil, 2*time.Second)
	
	chann, ok := rmq.GetConsumerChan("logPrefix", "q2207")
	if !ok {
		t.Error("не нашли очередь")
	}
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		var i = 0
		for {
			select {
			case m := <-chann:
				fmt.Printf("получили из q2207: %s\n", string(m.Body))
				if i == 0 {
					fmt.Println("i=0 => Nack")
					if e := m.Nack(false, true); e != nil {
						t.Error(e.Error())
					}
					i++
				} else {
					fmt.Println("i!=0 => Ack")
					if e := m.Ack(false); e != nil {
						t.Error(e.Error())
					}
					wg.Done()
					return
				}
			}
		}
	}()
	wg.Wait()
	
	rmq.Done("logPrefix")
	time.Sleep(5 * time.Second)
	
}

func Test_ThoroughlySendWPriority(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207.w.p": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207.w.p",
      "qos": 10,
      "args": {
        "x-max-priority": 10
      }
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	obj := amqp.Publishing{
		Priority: 111,
		Body:     []byte("текст какой=то"),
	}
	
	rmq.ThoroughlySendPublishing(ee.NewPrefix(), "q2207.w.p", obj, 2*time.Second)
	
	chann, ok := rmq.GetConsumerChan("logPrefix", "q2207.w.p")
	if !ok {
		t.Error("не нашли очередь")
	}
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		var i = 0
		for {
			select {
			case m := <-chann:
				fmt.Printf("получили из q2207.w.p: %s\n", string(m.Body))
				if i == 0 {
					fmt.Println("i=0 => Nack")
					if e := m.Nack(false, true); e != nil {
						t.Error(e.Error())
					}
					i++
				} else {
					fmt.Println("i!=0 => Ack")
					if e := m.Ack(false); e != nil {
						t.Error(e.Error())
					}
					wg.Done()
					return
				}
			}
		}
	}()
	wg.Wait()
	
	rmq.Done("logPrefix")
	time.Sleep(time.Second)
	
}

func Test_SendWithExpiration(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207.expiration": {
      "consume": true,
      "durable": false,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207.expiration",
      "qos": 10
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	obj := amqp.Publishing{
		Priority:   111,
		Body:       []byte("текст какой=то #1"),
		Expiration: "10000",
	}
	a := make(map[string]interface{})
	a["expiration"] = "10000"
	rmq.ThoroughlySendPublishing(ee.NewPrefix(), "q2207.expiration", obj, 2*time.Second)
	rmq.ThoroughlySend(ee.NewPrefix(), "q2207.expiration", []byte("текст какой=то#2"), nil, 2*time.Second)
	rmq.ThoroughlySend(ee.NewPrefix(), "q2207.expiration", []byte("текст какой=то #3"), nil, 2*time.Second, a)
	rmq.ThoroughlySendWithExpiration(ee.NewPrefix(), "q2207.expiration", []byte("текст какой=то #4"), nil, 2*time.Second, 10000)
	rmq.SendMsg(ee.NewPrefix(), "q2207.expiration", "", []byte("текст какой=то #5"), nil)
	rmq.SendMsg(ee.NewPrefix(), "q2207.expiration", "", []byte("текст какой=то #6"), nil, a)
	rmq.SendMsgWithExpiration(ee.NewPrefix(), "q2207.expiration", "", []byte("текст какой=то #7"), nil, 10000)
	
	// chann, ok := rmq.GetConsumerChan("logPrefix", "q2207.expiration")
	// if !ok {
	// 	t.Error("не нашли очередь")
	// }
	// wg := new(sync.WaitGroup)
	// wg.Add(1)
	// go func() {
	// 	var i = 0
	// 	for {
	// 		select {
	// 		case m := <-chann:
	// 			fmt.Printf("получили из q2207.w.p: %s\n", string(m.Body))
	// 			if i == 0 {
	// 				fmt.Println("i=0 => Nack")
	// 				if e := m.Nack(false, true); e != nil {
	// 					t.Error(e.Error())
	// 				}
	// 				i++
	// 			} else {
	// 				fmt.Println("i!=0 => Ack")
	// 				if e := m.Ack(false); e != nil {
	// 					t.Error(e.Error())
	// 				}
	// 				wg.Done()
	// 				return
	// 			}
	// 		}
	// 	}
	// }()
	// wg.Wait()
	//
	rmq.Done("logPrefix")
	time.Sleep(time.Second)
	
}

func TestOnce(t *testing.T) {
	begin(7)
	
	strCfg := `
{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207.once": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207.once",
      "qos": 10
    }
  }
}`
	logPrefix := ee.NewPrefix()
	
	cfg, err := ParseConfig(logPrefix, []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init(logPrefix, cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	mm := make(map[string]bool)
	for i := 0; i < 10; i++ {
		m := time.Now().Format(time.RFC3339Nano)
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Шлем " + m))
		if err := rmq.SendMsg(logPrefix, "q2207.once", "", []byte(m), nil); err != nil {
			t.Error("Error#1=" + err.Error())
			return
		}
		mm[m] = true
	}
	
	<-time.After(20 * time.Second)
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("закончили ждать"))
	
	done := make(chan struct{})
	
	c, ok := rmq.GetConsumerChan(logPrefix, "q2207.once")
	if !ok {
		t.Error("Error#2= нет консьюмера")
		return
	}
	
	for i := 0; i < 3; i++ {
		
		go func(idx int, doneChan chan struct{}, workChan chan *Message) {
			lp := ee.AddPrefix(logPrefix, "#", strconv.Itoa(idx))
			for {
				select {
				case msg := <-workChan:
					if _, ok := mm[string(msg.Body)]; !ok {
						t.Error("Error#3= нет msg=" + string(msg.Body))
						return
					}
					ll.Log(ee.Debug().WithPrefix(lp).WithMessage("Получили сообщение:" + string(msg.Body)))
					msg.Ack(false)
				case <-doneChan:
					ll.Log(ee.Debug().WithPrefix(lp).WithMessage("Выходим"))
				}
			}
		}(i, done, c)
		
	}
	
	<-time.After(time.Second)
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("закончили ждать"))
}

func TestNackOrder(t *testing.T) {
	begin(3)
	
	strCfg := `
{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207.once": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207.once",
      "qos": 2
    }
  }
}`
	logPrefix := ee.NewPrefix()
	
	cfg, err := ParseConfig(logPrefix, []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init(logPrefix, cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	rmq.SendMsg(logPrefix, "q2207.once", "", []byte("one"), nil)
	rmq.SendMsg(logPrefix, "q2207.once", "", []byte("two"), nil)
	
	// <-time.After(20 * time.Second)
	// ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("закончили ждать"))
	
	done := make(chan struct{})
	
	c, ok := rmq.GetConsumerChan(logPrefix, "q2207.once")
	if !ok {
		t.Error("Error#2= нет консьюмера")
		return
	}
	
	qos := cfg.Queues["q2207.once"].QOS
	
	go func() {
		for {
			select {
			case msg := <-c:
				got := string(msg.Body)
				fmt.Printf("при qos=%d, получили %s\n", qos, got)
				msg.Nack(false, true)
			case <-done:
				ll.Log(ee.Debug().WithMessage("Выходим"))
			}
		}
	}()
	
	<-time.After(30 * time.Second)
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("закончили ждать"))
}
