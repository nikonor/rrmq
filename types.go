package rrmq

import (
	"errors"
	"sync"
	
	"github.com/assembla/cony"
)

const (
	errServerDown string = "Не удалось соединиться с сервером RabbitMQ с попытки #%d"
)

// public errors
var (
	ErrHasNotQueue   = errors.New("Очередь не определена")
	ErrDuplicateKeys = errors.New("В конфигурации не все ключи уникальны")
	ErrNoConn        = errors.New("не удалось соединиться с сервером RMQ")
)

type consumerObject struct {
	sync.Mutex
	started   bool
	cli       *cony.Client
	cns       *cony.Consumer
	ch        chan *Message
	autoAck   bool
	queueName string
	url       string
	stopChan  chan struct{}
	
	consumeMapCH chan struct{}
}

type Obj struct {
	sync.RWMutex
	cfg           Config
	cli           *cony.Client
	publishers    map[string]*cony.Publisher
	customers     map[string]*consumerObject
	queues        map[string]*cony.Queue
	exchanges     map[string]*cony.Exchange
	bindings      []*cony.Binding
	declarations  []cony.Declaration
	doneChan      chan struct{}
	errorChan     chan string
	reconnectChan chan struct{}
	
	consumeMap         map[string]chan struct{}
	consumeStopChanMap map[string]chan struct{}
	consumeChanMap     map[string]chan *Message
}
