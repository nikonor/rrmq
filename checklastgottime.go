package rrmq

import (
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

const TimeFormat = "2006-01-02 15:04:05-07:00"

// CheckLastGotTime - проверя, не стоит ли переждать?
//      возвращает true, если можно продолжать
func CheckLastGotTime(logPrefix ee.LogPrefixType, timeOut int, m *Message) bool {
	var (
		fName, fnName = "checklastgottime.go", "CheckLastGotTime"
	)
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("вызов " + fName + "." + fnName))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("окончание " + fName + "." + fnName))
	}()
	
	if dt, ok := m.Headers["datetime"]; ok {
		
		t, _ := time.Parse(TimeFormat, dt.(string))
		tt := t.Add(time.Duration(timeOut-10) * time.Second)
		now := time.Now()
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("dt=", dt.(string), ", dt+1min=", tt.Format(TimeFormat), ", now=", now.Format(TimeFormat)))
		
		if tt.Before(now) {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("возвращаем true, т.е. нужно поспать"))
			return false
		}
	}
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("возвращаем true, т.е. можно продолжать"))
	return true
}
