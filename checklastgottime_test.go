package rrmq

import (
	"testing"
	"time"
	
	"github.com/streadway/amqp"
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

func TestCheckLastGotTime(t *testing.T) {
	ll.Start(7)
	ll.StartStdoutStream()
	
	type args struct {
		logPrefix ee.LogPrefixType
		timeOut   int
		m         *Message
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "minute",
			args: args{
				logPrefix: "minute",
				timeOut:   60,
				m: &Message{
					Acknowledger:    nil,
					Headers:         amqp.Table(map[string]interface{}{"datetime": time.Now().Format(TimeFormat)}),
					ContentType:     "",
					ContentEncoding: "",
					DeliveryMode:    0,
					Priority:        0,
					CorrelationId:   "",
					ReplyTo:         "",
					Expiration:      "",
					MessageId:       "",
					Timestamp:       time.Time{},
					Type:            "",
					UserId:          "",
					AppId:           "",
					ConsumerTag:     "",
					MessageCount:    0,
					DeliveryTag:     0,
					Redelivered:     false,
					Exchange:        "",
					RoutingKey:      "",
					Body:            []byte("body"),
				},
			},
			want: true,
		},
		{
			name: "second",
			args: args{
				logPrefix: "second",
				timeOut:   1,
				m: &Message{
					Acknowledger:    nil,
					Headers:         amqp.Table(map[string]interface{}{"datetime": time.Now().Format(TimeFormat)}),
					ContentType:     "",
					ContentEncoding: "",
					DeliveryMode:    0,
					Priority:        0,
					CorrelationId:   "",
					ReplyTo:         "",
					Expiration:      "",
					MessageId:       "",
					Timestamp:       time.Time{},
					Type:            "",
					UserId:          "",
					AppId:           "",
					ConsumerTag:     "",
					MessageCount:    0,
					DeliveryTag:     0,
					Redelivered:     false,
					Exchange:        "",
					RoutingKey:      "",
					Body:            []byte("body"),
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CheckLastGotTime(tt.args.logPrefix, tt.args.timeOut, tt.args.m); got != tt.want {
				t.Errorf("CheckLastGotTime() = %v, want %v", got, tt.want)
			}
		})
	}
}
