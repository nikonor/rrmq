package rrmq

import (
	"fmt"
	"strconv"
	"sync"
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
)

var started bool

func begin(level int) {
	if !started {
		ll.Start(level)
		// Логирование в syslog
		ll.StartStdoutStream()
		started = true
	}
}

func TestParseConfig2(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.1",
      "qos": 10
    },
    "queue2": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.2",
      "qos": 10
    },
    "queue3": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.3",
      "qos": 10
    }
  },
  "exchanges": {
    "fff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false
    }
  },
  "bindings": [
    {
      "queue": "queue1",
      "exchange": "fff"
    },
    {
      "queue": "queue2",
      "exchange": "fff"
    }
  ]
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("cfg=%#v\n", cfg)
}

func TestInit2(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.1",
      "qos": 10
    },
    "queue2": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.2",
      "qos": 10,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    },
    "queue3": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.3",
      "qos": 10
    }
  },
  "exchanges": {
    "ffff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "bindings": [
    {
      "queue": "queue1",
      "exchange": "ffff"
    },
    {
      "queue": "queue2",
      "exchange": "ffff"
    }
  ]
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	// fmt.Printf("cfg=%#v\n", cfg)
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
	}
	
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	for i := 0; i < 100; i++ {
		err := rmq.SendMsg("logPrefix", "fff", "application/json", []byte(`{"target":"fff","one":`+strconv.Itoa(i+1)+`,"two":`+strconv.Itoa(i+1001)+`}`), nil)
		if err != nil {
			t.Error(i, err)
		}
		// time.Sleep(time.Millisecond)
	}
	
	rmq.SendMsg("logPrefix", "queue1", "application/json", []byte(`{"target":"queue1","one":11,"two":22}`), nil)
	rmq.SendMsg("logPrefix", "queue2", "application/json", []byte(`{"target":"queue2","one":11,"two":22}`), nil)
	rmq.SendMsg("logPrefix", "queue3", "application/json", []byte(`{"target":"queue3","one":11,"two":22}`), nil)
	//
	// time.Sleep(time.Second)
	
	ch1, ok := rmq.GetConsumerChan("logPrefix", "queue1")
	if !ok {
		fmt.Printf("Error: ch1")
		return
	}
	ch2, ok := rmq.GetConsumerChan("logPrefix", "queue2")
	if !ok {
		fmt.Printf("Error: ch2")
		return
	}
	ch3, ok := rmq.GetConsumerChan("logPrefix", "queue3")
	if !ok {
		fmt.Printf("Error: ch3")
		return
	}
	dch := make(chan struct{})
	for i := 0; i < 3; i++ {
		go func(idx int, ch1 chan *Message, ch2 chan *Message, ch3 chan *Message) {
			for {
				select {
				case <-dch:
					println("was closed")
					return
				case msg := <-ch1:
					if msg != nil {
						fmt.Printf("Worker #%d получил из queue1: %#v\n", idx, msg)
						msg.Ack(false)
					} else {
						fmt.Printf("Worker #%d получил из queue1 NIL\n", idx)
					}
				case msg := <-ch2:
					if msg != nil {
						fmt.Printf("Worker #%d получил из queue2: %#v\n", idx, msg)
						msg.Ack(false)
					} else {
						fmt.Printf("Worker #%d получил из queue2 NIL\n", idx)
					}
				case msg := <-ch3:
					if msg != nil {
						fmt.Printf("Worker #%d получил из queue3: %#v\n", idx, msg)
						msg.Ack(false)
					} else {
						fmt.Printf("Worker #%d получил из queue3 NIL\n", idx)
					}
				}
			}
		}(i, ch1, ch2, ch3)
	}
	
	time.Sleep(3 * time.Second)
	rmq.Done("logPrefix")
	close(dch)
}

func TestOneMsg(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.4",
      "qos": 10
    },
    "queue2": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.3",
      "qos": 10,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("abc"), nil); err != nil {
		t.Error(err)
	}
	if err = rmq.SendMsg("logPrefix", "queue2", "", []byte("QWE"), nil); err != nil {
		t.Error(err)
	}
	rmq.Done("logPrefix")
	
}

func TestError404(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.404",
      "qos": 10
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
				// if strings.Contains(s, "404") {
				// go rmq.Reconnect("QQQQ")
				// }
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("abc::"+time.Now().Format(time.RFC3339)), nil); err != nil {
		t.Error(err)
	}
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("qwe::"+time.Now().Format(time.RFC3339)), nil); err != nil {
		t.Error(err)
	}
	
	tt := time.Duration(10)
	
	channel, ok := rmq.GetConsumerChan("logPrefix", "queue1")
	if !ok {
		t.Error("Не получили channel")
		return
	}
	
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("asd::"+time.Now().Format(time.RFC3339)), nil); err != nil {
		t.Error(err)
	}
	
	ticker := time.NewTicker(tt * time.Second)
	for {
		select {
		case <-ticker.C:
			fmt.Printf("Прошло %d сек. Завершаемся\n", int64(tt))
			return
		case m := <-channel:
			fmt.Printf("\nПолучено сообщение (type=%T): %s\n", m, string(m.Body))
			m.Ack(false)
		}
	}
	
	time.Sleep(tt)
	rmq.Done("logPrefix")
	
}

func TestOneMsgToExch(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "ffff.aaa.1",
      "qos": 10
    },
    "queue2": {
      "consume": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "ffff.aaa.2",
      "qos": 10,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "exchanges": {
    "fff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "bindings": [
    {
      "queue": "queue1",
      "exchange": "fff"
    },
    {
      "queue": "queue2",
      "exchange": "fff"
    }
   ]
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "fff", "", []byte("abc"), nil); err != nil {
		t.Error(err)
	}
	rmq.Done("logPrefix")
	
}

func TestResend(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.4",
      "qos": 10
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("now="+time.Now().Format(timeFormat)), nil); err != nil {
		t.Error(err)
	}
	
	chann, ok := rmq.GetConsumerChan("logPrefix", "queue1")
	if !ok {
		t.Error("не нашли очередь")
	}
	m := <-chann
	
	fmt.Printf("получили из queue1: " + string(m.Body))
	m.Ack(false)
	
	rmq.Done("logPrefix")
	
}

func TestInit3(t *testing.T) {
	begin(7)
	strCfg1 := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.1",
      "qos": 10
    }
  },
  "exchanges": {
    "fff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "bindings": [
    {
      "queue": "wrongQueue",
      "exchange": "ffff"
    }
  ]
}`
	strCfg2 := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.1",
      "qos": 10
    }
  },
  "exchanges": {
    "fff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "bindings": [
    {
      "queue": "queue1",
      "exchange": "wrongExchange"
    }
  ]
}`
	
	strCfg3 := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "queue1": {
      "consume": true,
      "autoAck": false,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "fff.aaa.1",
      "qos": 10
    }
  },
  "exchanges": {
    "fff": {
      "name":"ffff",
      "type": "fanout",
      "durable": true,
      "autoDelete": false,
      "args": {
        "ttl": 10,
        "qwe" : "asd"
      }
    }
  },
  "bindings": [
    {
      "queue": "queue1",
      "exchange": "fff"
    }
  ]
}`
	
	cfg, err := ParseConfig("logPrefix", []byte(strCfg1))
	if err != nil {
		t.Error(err)
		return
	}
	// fmt.Printf("cfg=%#v\n", cfg)
	_, err = Init("logPrefix", cfg)
	if err == nil || err.Error() != "Obj::Ошибка биндинга. Очередь wrongQueue не определена" {
		t.Error("Ошибка #1")
	}
	
	cfg, err = ParseConfig("logPrefix", []byte(strCfg2))
	if err != nil {
		t.Error(err)
		return
	}
	_, err = Init("logPrefix", cfg)
	if err == nil || err.Error() != "Obj::Ошибка биндинга. Exchange wrongExchange не определен" {
		t.Error("Ошибка #2")
	}
	
	cfg, err = ParseConfig("logPrefix", []byte(strCfg3))
	if err != nil {
		t.Error(err)
		return
	}
	_, err = Init("logPrefix", cfg)
	if err != nil {
		t.Error("Ошибка #2:", err)
	}
	
}

func Test2207(t *testing.T) {
	begin(7)
	strCfg := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207",
      "qos": 10
    }
  }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
		return
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "q2207", "", []byte("now for 2207="+time.Now().Format(timeFormat)), nil); err != nil {
		t.Error(err)
	}
	
	chann, ok := rmq.GetConsumerChan("logPrefix", "q2207")
	if !ok {
		t.Error("не нашли очередь")
	}
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		var i = 0
		for {
			select {
			case m := <-chann:
				fmt.Printf("получили из q2207: %s\n", string(m.Body))
				if i == 0 {
					fmt.Println("i=0 => Nack")
					if e := m.Nack(false, true); e != nil {
						t.Error(e.Error())
					}
					i++
				} else {
					fmt.Println("i!=0 => Ack")
					if e := m.Ack(false); e != nil {
						t.Error(e.Error())
					}
					wg.Done()
					return
				}
			}
		}
	}()
	wg.Wait()
	
	rmq.Done("logPrefix")
	time.Sleep(time.Second)
	
}

func TestInitWithXMessageTTL(t *testing.T) {
	println("Если вы не удалили очередь fff.aaa.wait, то тест не сработает")
	begin(7)
	strCfg := `
{
    "server": {
        "addr": "localhost",
        "port": 5672,
        "login": "guest",
        "password": "guest"
    },
    "queues": {
        "queue1": {
            "qos": 10,
            "name": "fff.aaa.wait",
            "consume": false,
            "durable": true,
            "exclusive": false,
            "autoDelete": false,
            "args": {
                "x-dead-letter-routing-key": "not.wait",
                "x-dead-letter-exchange": "",
                "x-message-ttl": 5000
            }
        }
    }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	// fmt.Printf("cfg=%#v\n", cfg)
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	if err = rmq.SendMsg("logPrefix", "queue1", "", []byte("QQQQQ"), nil); err != nil {
		t.Error(err)
	}
	
	time.Sleep(3 * time.Second)
	rmq.Done("logPrefix")
}

func TestLong(t *testing.T) {
	begin(7)
	strCfg := `
{
    "server": {
        "addr": "localhost",
        "port": 5672,
        "login": "guest",
        "password": "guest"
    },
    "queues": {
        "queue1": {
            "qos": 10,
            "name": "fff.aaa.long",
            "consume": true,
            "durable": true,
            "exclusive": false,
            "autoDelete": false
        }
    }
}`
	cfg, err := ParseConfig("logPrefix", []byte(strCfg))
	if err != nil {
		t.Error(err)
		return
	}
	// fmt.Printf("cfg=%#v\n", cfg)
	rmq, err := Init("logPrefix", cfg)
	if err != nil {
		t.Error(err)
	}
	go func(rmq *Obj) {
		for {
			select {
			case s := <-rmq.ErrorChan():
				ll.Log(ee.Error().WithMessage("Ошибка в RMQ:" + s))
			case <-rmq.DoneChan():
				ll.Log(ee.Debug().WithMessage("Завершаем работу с RMQ"))
				return
			}
		}
	}(rmq)
	
	go func() {
		for {
			s := strconv.FormatInt(time.Now().UnixNano(), 10)
			if err := rmq.SendMsg("logPrefixCLI", "queue1", "", []byte(s), nil); err != nil {
				t.Error(err)
			}
			time.Sleep(3 * time.Second)
		}
	}()
	
	go func() {
		q, _ := rmq.GetConsumerChan("logPrefixCNS", "queue1")
		for {
			select {
			case msg := <-q:
				fmt.Printf("Получили из очереди: %s\n", string(msg.Body))
				msg.Ack(false)
			}
		}
	}()
	
	time.Sleep(time.Hour)
	rmq.Done("logPrefix")
}
