module gitlab.com/nikonor/rrmq

go 1.18

require (
	github.com/assembla/cony v0.3.2
	github.com/streadway/amqp v1.0.0
	gitlab.com/nikonor/eerrors v1.0.0
	gitlab.com/nikonor/llog v1.0.0
	gitlab.com/nikonor/throttling v1.0.0
)
