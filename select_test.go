package rrmq

import (
	"fmt"
	"testing"
	"time"
	
	ee "gitlab.com/nikonor/eerrors"
)

func Test_Select(t *testing.T) {
	begin(7)
	
	strCfgWConsumer := `{
  "server": {
    "addr": "localhost",
    "port": 5672,
    "login": "guest",
    "password": "guest"
  },
  "queues": {
    "q2207": {
      "consume": true,
      "durable": true,
      "exclusive": false,
      "autoDelete": false,
      "name": "q2207",
      "qos": 10
    }
  }
}`
	
	logPrefix := ee.NewPrefix()
	
	cfg, err := ParseConfig("logPrefix", []byte(strCfgWConsumer))
	if err != nil {
		t.Error(err)
		return
	}
	
	rmq1, err := GetRMQ(logPrefix, cfg)
	
	ch, _ := rmq1.GetConsumerChan(logPrefix, "q2207")
	
	for i := 0; i <= 20; i++ {
		body := []byte("test string::" + time.Now().Format(time.RFC3339))
		
		rmq1.ThoroughlySend(logPrefix, "q2207", body, nil, time.Second)
	}
	
	msg := <-ch
	fmt.Printf("got=%s\n", string(msg.Body))
	// if bytes.Compare(msg.Body, body) != 0 {
	// 	t.Error("Ошибка", string(msg.Body), "<===>", string(body))
	// }
	msg.Ack(false)
	//
	// // time.Sleep(time.Second)
	
	time.Sleep(1 * time.Second)
	rmq1.Done(logPrefix)
	
}
