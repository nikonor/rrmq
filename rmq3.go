package rrmq

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"
	
	"github.com/assembla/cony"
	"github.com/streadway/amqp"
	ee "gitlab.com/nikonor/eerrors"
	ll "gitlab.com/nikonor/llog"
	"gitlab.com/nikonor/throttling"
)

// имя файла для логирования
var (
	logFile = "rmq3.go"
	// consumeMap         map[string]chan struct{}
	// consumeStopChanMap map[string]chan struct{}
	// consumeChanMap     map[string]chan *Message
)

type Message amqp.Delivery

func (msg Message) Ack(multiple bool) error {
	mm := amqp.Delivery(msg)
	return mm.Ack(multiple)
}

func (msg Message) Nack(multiple bool, requeue bool) error {
	mm := amqp.Delivery(msg)
	return mm.Nack(multiple, requeue)
}

func (msg Message) Reject(requeue bool) error {
	mm := amqp.Delivery(msg)
	return mm.Reject(requeue)
	
}

func (msg Message) NackDelay(delay time.Duration, multiple bool, requeue bool) error {
	time.Sleep(delay)
	mm := amqp.Delivery(msg)
	return mm.Nack(multiple, requeue)
}

const defaultTimeout = 3 // максимальное время отправки. в секундах
const timeFormat = "2006-01-02 15:04:05-07:00"
const FieldDateTime = "Obj-datetime"
const FieldTryCount = "Obj-try-count"
const FieldConfigKey = "X-Emp-RMQ-Config-Key"

var timeout int

var tag TagT

func (r *Obj) SetApp(appname string) {
	tag.App = appname
}

// Init - создаем все и вся
func Init(logPrefix ee.LogPrefixType, cfg Config, timeoutParams ...int) (*Obj, error) {
	logFunc := "Init"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Инициализация конфигурации RabbitMQ").WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	
	var (
		err error
		r   Obj
	)
	
	tag = fillTag()
	
	r.Lock()
	defer r.Unlock()
	
	timeout = defaultTimeout
	if len(timeoutParams) == 1 {
		timeout = timeoutParams[0]
	}
	
	r.cfg = cfg
	
	r.doneChan = make(chan struct{})
	r.errorChan = make(chan string)
	r.reconnectChan = make(chan struct{})
	r.consumeMap = make(map[string]chan struct{})
	r.consumeStopChanMap = make(map[string]chan struct{})
	r.consumeChanMap = make(map[string]chan *Message)
	
	r.publishers = make(map[string]*cony.Publisher)
	r.customers = make(map[string]*consumerObject)
	r.queues = make(map[string]*cony.Queue)
	r.exchanges = make(map[string]*cony.Exchange)
	
	url := createURL(logPrefix, cfg)
	
	if r.cli, err = createClient(logPrefix, url); err != nil {
		return nil, err
	}
	
	// очереди
	for k, v := range cfg.Queues {
		// создаем и декларируем объект очереди
		r.queues[k] = &cony.Queue{
			Name:       v.Name,
			Durable:    v.Durable,
			AutoDelete: v.AutoDelete,
			Exclusive:  v.Exclusive,
		}
		if v.Args != nil {
			args := amqp.Table{}
			for ak, av := range v.Args {
				switch ak {
				case "x-message-ttl", "x-max-priority":
					args[ak] = int64(av.(float64))
				default:
					args[ak] = av
				}
				
			}
			r.queues[k].Args = args
		}
		j, _ := json.Marshal(r.queues[k])
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Настройки очереди " + k + ":" + string(j)))
		r.declarations = append(r.declarations, cony.DeclareQueue(r.queues[k]))
		
		// сразу создаем паблишера
		r.publishers[k] = cony.NewPublisher("", v.Name)
		r.cli.Publish(r.publishers[k])
		
		// если из очереди предполагается потреблять, создаем консьюмера
		if v.Consume {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Создание консьюмера для очереди", v.Name).WithAt(logFile, logFunc))
			
			r.consumeMap[v.Name] = make(chan struct{})
			r.consumeChanMap[v.Name] = make(chan *Message)
			r.consumeStopChanMap[v.Name] = make(chan struct{})
			
			c, err := newConsumer(logPrefix, url, v, r.errorChan, r.doneChan, v.QOS, v.AutoAck, r.consumeChanMap[v.Name], r.consumeStopChanMap[v.Name], r.consumeMap[v.Name])
			if err != nil {
				return nil, err
			}
			r.customers[k] = c
			
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Консьюмера для очереди", v.Name, "создан успешно").WithAt(logFile, logFunc))
		}
	}
	
	// exchanges
	for k, v := range cfg.Exchanges {
		r.publishers[k] = cony.NewPublisher(v.Name, "")
		r.cli.Publish(r.publishers[k])
		
		r.exchanges[k] = &cony.Exchange{
			Name:       v.Name,
			Kind:       v.Type,
			Durable:    v.Durable,
			AutoDelete: v.AutoDelete,
		}
		if v.Args != nil {
			args := amqp.Table{}
			for ak, av := range v.Args {
				args[ak] = av
			}
			r.exchanges[k].Args = args
		}
		j, _ := json.Marshal(r.exchanges[k])
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Настройки exchange " + k + ":" + string(j)))
		
		r.declarations = append(r.declarations, cony.DeclareExchange(*r.exchanges[k]))
	}
	
	// bindings
	for _, v := range cfg.Bindings {
		if _, ok := r.queues[v.Queue]; !ok {
			return nil, errors.New("Obj::Ошибка биндинга. Очередь " + v.Queue + " не определена")
		}
		if _, ok := r.exchanges[v.Exchange]; !ok {
			return nil, errors.New("Obj::Ошибка биндинга. Exchange " + v.Exchange + " не определен")
		}
		b := cony.Binding{
			Queue:    r.queues[v.Queue],
			Exchange: *r.exchanges[v.Exchange],
			Key:      r.queues[v.Queue].Name,
		}
		r.declarations = append(r.declarations, cony.DeclareBinding(b))
		r.bindings = append(r.bindings, &b)
	}
	
	r.cli.Declare(r.declarations)
	
	// запускаем основной цикл
	mainLoop(logPrefix, &r, false)
	
	// даем время осмыслить все
	time.Sleep(time.Second)
	
	return &r, nil
}

func fillTag() TagT {
	var t TagT
	t.Host, _ = os.Hostname()
	t.Pid = os.Getpid()
	tmp, _ := os.Executable()
	
	_, t.App = path.Split(tmp)
	
	return t
}

// GetConsumerChan - получение канала консьюмера
func (o *Obj) GetConsumerChan(logPrefix ee.LogPrefixType, key string) (chan *Message, bool) {
	o.customers[key].Lock()
	defer o.customers[key].Unlock()
	
	if !o.customers[key].started {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Стартуем консьмера"))
		go consumerLoop(logPrefix, o.cfg.Queues[key], o.customers[key], o.errorChan, o.doneChan)
		o.customers[key].started = true
	}
	
	c, ok := o.customers[key]
	if !ok {
		return nil, ok
	}
	return c.ch, true
}

func (o *Obj) SendMsgWithExpiration(logPrefix ee.LogPrefixType, key, ct string, msg []byte, headers amqp.Table, expiration int64) error {
	a := make(map[string]interface{})
	a["expiration"] = strconv.FormatInt(expiration, 10)
	return o.SendMsg(logPrefix, key, ct, msg, headers, a)
}

// SendMsg - отправка сообщения
//      ключ - ключ очереди/эксченджа
//      ct - content-type, м.б. пустой строкой, если не нужно
//      msg - тело сообщения
//      headers - словарь типа amqp.Table (map[string]interface{}, хотя в реальности map[string]string)
func (o *Obj) SendMsg(logPrefix ee.LogPrefixType, key, ct string, msg []byte, headers amqp.Table, additional ...map[string]interface{}) error {
	var ok bool
	headers = updateHeaders(headers, key)
	
	obj := amqp.Publishing{
		Body:    msg,
		Headers: headers,
	}
	if len(additional) == 1 {
		for k, v := range additional[0] {
			k = strings.ToLower(k)
			// Properties
			switch k {
			case "contenttype":
				obj.ContentType, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить ContentType"))
				}
			case "contentencoding":
				obj.ContentEncoding, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить ContentEncoding"))
				}
			case "deliverymode":
				obj.DeliveryMode, ok = v.(uint8)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить DeliveryMode"))
				}
				// uint8     // Transient (0 or 1) or Persistent (2)
			case "priority":
				// uint8     // 0 to 9
				obj.Priority, ok = v.(uint8)
				if !ok || (ok && obj.Priority > 9) {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить Priority"))
				}
			case "correlationid":
				// string    // correlation identifier
				obj.CorrelationId, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить CorrelationId"))
				}
			case "replyto":
				// string    // address to to reply to (ex: RPC)
				obj.ReplyTo, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить ReplyTo"))
				}
			case "expiration":
				// string    // message expiration spec
				obj.Expiration, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить Expiration"))
				}
			case "messageid":
				// string    // message identifier
				obj.MessageId, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить MessageId"))
				}
			case "timestamp":
				obj.Timestamp, ok = v.(time.Time)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить Timestamp"))
				}
				// time.Time // message timestamp
			case "type":
				obj.Type, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить Type"))
				}
				// string    // message type name
			case "userid":
				obj.UserId, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить UserId"))
				}
				// string    // creating user id - ex: "guest"
			case "appid":
				obj.AppId, ok = v.(string)
				if !ok {
					ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Ну удалось раскастить AppId"))
				}
				// string    // creating application id
			}
			
		}
	}
	
	if len(ct) > 0 {
		obj.ContentType = ct
	}
	
	return o.Send(logPrefix, key, obj)
}

// Send - отправка сообщения
//      ключ - ключ очереди/эксченджа
//      ct - content-type, м.б. пустой строкой, если не нужно
//      msg - тело сообщения
//      headers - словарь типа amqp.Table (map[string]interface{}, хотя в реальности map[string]string)
func (o *Obj) Send(logPrefix ee.LogPrefixType, key string, obj amqp.Publishing) error {
	logFunc := "Send"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("старт").WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	
	var (
		wg     sync.WaitGroup
		retErr error
	)
	
	o.RLock()
	defer o.RUnlock()
	
	pbl, ok := o.publishers[key]
	if !ok {
		return ErrHasNotQueue
	}
	
	// ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	// ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Nanosecond)
	
	e := make(chan error)
	q := make(chan struct{})
	// defer func(logPrefix ee.LogPrefixType, e chan error, q chan struct{}, f context.CancelFunc) {
	defer func(logPrefix ee.LogPrefixType, e chan error, q chan struct{}) {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("defer"))
		close(e)
		close(q)
		// cancel()
	}(logPrefix, e, q)
	// }(logPrefix, e, q, cancel)
	
	wg.Add(1)
	
	go func(logPrefix ee.LogPrefixType, wg *sync.WaitGroup) {
		defer func(wg *sync.WaitGroup) {
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("bg defer"))
			wg.Done()
		}(wg)
		
		defer func() {
			if err := recover(); err != nil {
				ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+logFunc+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
			}
		}()
		
		for {
			select {
			// case <-ctx.Done():
			// 	o.errorChan <- "timeout::не удалось отправить сообщение"
			// 	return
			case retErr = <-e:
				o.errorChan <- retErr.Error()
				return
			case <-q:
				return
			}
		}
	}(logPrefix, &wg)
	
	if ll.GetLogLevel() == ee.DEBUG {
		kind, to := o.getDstName(key)
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage(
			"Отправка в", kind, to, "сообщения", string(obj.Body), "с заголовками", obj.Headers,
		).WithAt(logFile, logFunc))
	}
	
	go send(pbl, obj, e, q)
	
	wg.Wait()
	
	return retErr
}

// send - публикация сообщения
func send(pbl *cony.Publisher, obj amqp.Publishing, e chan error, q chan struct{}) {
	
	defer func() {
		if err := recover(); err != nil {
			ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+"send"+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
		}
	}()
	
	if err := pbl.Publish(obj); err != nil {
		// fmt.Printf("%#v\n", err)
		if err != cony.ErrPublisherDead {
			e <- err
		}
		return
	}
	
	q <- struct{}{}
}

func updateHeaders(in amqp.Table, key string) amqp.Table {
	var wasTryCount bool
	
	out := amqp.Table{}
	
	for k, v := range in {
		out[k] = v
		if k == FieldTryCount {
			out[k] = v.(int64) + 1
			wasTryCount = true
		}
	}
	out[FieldDateTime] = time.Now().Format(timeFormat)
	if !wasTryCount {
		out[FieldTryCount] = int64(0)
	}
	out[FieldConfigKey] = key
	
	return out
}

func (o *Obj) GetDateTimeFieldName() string {
	return FieldDateTime
}

func (o *Obj) GetTryCountFieldName() string {
	return FieldTryCount
}

// ParseConfig - парсинг конфига
func ParseConfig(logPrefix ee.LogPrefixType, in []byte) (Config, error) {
	logFunc := "ParseConfig"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Чтение конфигурации RabbitMQ из JSON-объекта").WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	
	cfg := Config{}
	
	if err := json.Unmarshal(in, &cfg); err != nil {
		return cfg, err
	}
	
	// проверяем уникальность ключей в JSON-объекте настроек
	if !checkKeys(logPrefix, cfg) {
		return cfg, ErrDuplicateKeys
	}
	
	return cfg, nil
}

// Reconnect - перезапуск соединения и консьюмеров
func (o *Obj) Reconnect(logPrefix ee.LogPrefixType) {
	
	logFunc := "Reconnect"
	
	defer func() {
		if err := recover(); err != nil {
			ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+"Reconnect"+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
		}
	}()
	
	if err := o.reCreateClient(logPrefix); err != nil {
		ll.Log(ee.Error().WithPrefix(logPrefix).WithAt(logFile, logFunc).WithMessage("ошибка при перезапуске подключения к RMQ"))
	}
	
	mainLoop(logPrefix, o, true)
}

// reCreateClient - пересоздание клиента подключения к RabbitMQ
func (o *Obj) reCreateClient(logPrefix ee.LogPrefixType) error {
	logFunc := "reCreateClient"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("старт").WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	o.RLock()
	defer o.RUnlock()
	
	url := createURL(logPrefix, o.cfg)
	
	newClient, err := createClient(logPrefix, url)
	if err != nil {
		return err
	}
	o.cli.Close()
	newClient.Declare(o.declarations)
	for _, v := range o.publishers {
		newClient.Publish(v)
	}
	o.cli = newClient
	
	return nil
}

// Done - закрытие канала doneChan и завершение работы
func (o *Obj) Done(logPrefix ee.LogPrefixType) {
	close(o.doneChan)
	return
}

// done - завершение работы
func (o *Obj) done(logPrefix ee.LogPrefixType) {
	o.Lock()
	defer o.Unlock()
	
	// читатели
	for _, v := range o.customers {
		v.cns.Cancel()
		close(v.ch)
	}
	
	// писатели
	for _, v := range o.publishers {
		v.Cancel()
	}
	
	o.cli.Close()
}

// DoneChan - возвращает канал завершения
func (o *Obj) DoneChan() chan struct{} {
	return o.doneChan
}

// ErrorChan - возвращает канал ошибок
func (o *Obj) ErrorChan() chan string {
	return o.errorChan
}

// // ReconnectChan - возвращает канал переподключения
// func (o *Obj) ReconnectChan() chan struct{} {
// 	return o.reconnectChan
// }

// mainLoop - основной цикл: проверка соединения
func mainLoop(logPrefix ee.LogPrefixType, r *Obj, reconnect bool) {
	logFunc := "mainLoop"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("старт").WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	
	url := createURL(logPrefix, r.cfg)
	
	numTries := 1
	backoffTime := 1
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Попытка соединиться с сервером RabbitMQ по адресу", r.cfg.Server.Addr, ":", r.cfg.Server.Port).WithAt(logFile, logFunc))
	
	for {
		err := checkRMQ(logPrefix, url)
		if err != nil {
			// send error to application
			r.errorChan <- fmt.Sprintf(errServerDown, numTries)
			
			// pausing
			time.Sleep(time.Duration(backoffTime) * time.Second)
			
			// backoff
			backoffTime++
			numTries++
			
			if backoffTime > 10 {
				backoffTime = 10
			}
		} else {
			break
		}
	}
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage(
		"Соединение с сервером RabbitMQ по адресу", r.cfg.Server.Addr, ":", r.cfg.Server.Port, "установлено. Работа ведется в штатном режиме.",
	).WithAt(logFile, logFunc))
	
	go func() {
		
		logFuncInternal := logFunc + "::reconnectChanWatcher"
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt(logFile, logFuncInternal).
			WithMessage("Запуск рутины отслеживания необходимости переподключения"))
		
		defer func() {
			if err := recover(); err != nil {
				ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+logFunc+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
			}
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt(logFile, logFuncInternal).
				WithMessage("Завершение рутины отслеживания необходимости переподключения"))
		}()
		
		for {
			select {
			case <-r.reconnectChan:
				ll.Log(ee.Info().WithPrefix(logPrefix).WithAt(logFile, logFuncInternal).
					WithMessage("Запуск процесса переподключения к RMQ"))
				go r.Reconnect(logPrefix)
				return
			case <-r.doneChan:
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt(logFile, logFuncInternal).
					WithMessage("Получена команда на завершение взаимодействия с RMQ"))
				return
			}
		}
	}()
	
	go func() {
		
		defer func() {
			if err := recover(); err != nil {
				ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+logFunc+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
			}
		}()
		
		for r.cli.Loop() {
			select {
			case err := <-r.cli.Errors():
				// Если ошибка, и это пропал коннект
				if ei, ok := err.(*amqp.Error); ok && ei.Code == 501 {
					r.reconnectChan <- struct{}{}
					return
				}
				r.errorChan <- err.Error()
			case blocked := <-r.cli.Blocking():
				r.errorChan <- blocked.Reason
				if blocked.Active {
					ll.Log(ee.Info().WithPrefix(logPrefix).WithAt(logFile, logFunc).
						WithMessage(blocked.Reason + " соединение блокировано со стороны сервера. Спим секунду"))
					time.Sleep(time.Second)
				}
			case <-r.doneChan:
				r.done(logPrefix)
				return
			}
		}
	}()
	
	if reconnect {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Перезапуск консьюмеров"))
		
		for k, v := range r.customers {
			// останавливаем консьюмера
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt(logFile, logFunc).WithMessage("отправляем команду на перезапуск потребителя для " + v.queueName))
			v.stopChan <- struct{}{}
			// перезапускаем консьюмера
			go v.reNewConsumer(logPrefix, r.cfg.Queues[k], r.errorChan, r.doneChan, r.cfg.Queues[k].QOS, v.autoAck, r.consumeChanMap[v.queueName], r.consumeStopChanMap[v.queueName])
		}
	}
}

// newConsumer - запуск нового консьюмера
func newConsumer(logPrefix ee.LogPrefixType, url string, queue ConfigQueue, eChan chan string, dChan chan struct{}, qos int, autoAck bool, consumeChan chan *Message, consumeStopChan chan struct{}, consumeMapCH chan struct{}) (*consumerObject, error) {
	logFunc := "newConsumer"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Запуск консьюмера для", queue.Name).WithAt(logFile, logFunc))
	defer func() {
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("конец").WithAt(logFile, logFunc))
	}()
	
	cli, err := createClient(logPrefix, url)
	if err != nil {
		return nil, err
	}
	
	q := &cony.Queue{
		Name:       queue.Name,
		Durable:    queue.Durable,
		AutoDelete: queue.AutoDelete,
		Exclusive:  queue.Exclusive,
	}
	
	tagStr := makeTag()
	
	ret := consumerObject{
		cns:          cony.NewConsumer(q, cony.Qos(qos), cony.Tag(tagStr)),
		ch:           consumeChan,
		cli:          cli,
		autoAck:      autoAck,
		queueName:    queue.Name,
		url:          url,
		stopChan:     consumeStopChan,
		consumeMapCH: consumeMapCH,
	}
	
	ret.cli.Consume(ret.cns)
	
	// go consumerLoop(logPrefix, queue, &ret, eChan, dChan)
	
	return &ret, nil
}

func makeTag() string {
	curTag := TagT{
		App:   tag.App,
		Pid:   tag.Pid,
		Host:  tag.Host,
		Since: time.Now().Format(time.RFC3339),
	}
	j, _ := json.Marshal(curTag)
	return string(j)
}

func (o *consumerObject) reNewConsumer(logPrefix ee.LogPrefixType, queue ConfigQueue, eChan chan string, dChan chan struct{}, qos int, autoAck bool, cCM chan *Message, cSCM chan struct{}) {
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов reNewConsumer"))
	defer func() { ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание reNewConsumer")) }()
	
	q := &cony.Queue{
		Name:       queue.Name,
		Durable:    queue.Durable,
		AutoDelete: queue.AutoDelete,
		Exclusive:  queue.Exclusive,
	}
	
	newCli, err := createClient(logPrefix, o.url)
	if err != nil {
		ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage(err))
	}
	
	ret := consumerObject{
		cns:          cony.NewConsumer(q, cony.Qos(qos)),
		ch:           cCM, // consumeChanMap[queue.Name],
		cli:          newCli,
		autoAck:      autoAck,
		queueName:    queue.Name,
		url:          o.url,
		stopChan:     cSCM, // consumeStopChanMap[queue.Name],
		consumeMapCH: o.consumeMapCH,
	}
	
	ret.cli.Consume(ret.cns)
	
	go consumerLoop(logPrefix, queue, &ret, eChan, dChan)
	
}

// consumeLoop - главный цикл консьюмера
func consumerLoop(logPrefix ee.LogPrefixType, queue ConfigQueue, o *consumerObject, eChan chan string, dChan chan struct{}) {
	logFunc := "consumerLoop"
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Вызов consumerLoop для " + o.queueName))
	defer func() {
		o.cns.Cancel()
		o.cli.Close()
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Окончание consumerLoop для " + o.queueName))
	}()
	
	defer func() {
		if err := recover(); err != nil {
			ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+logFunc+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
		}
	}()
	
	mCh := o.consumeMapCH
	
	go func(key string) {
		// ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Старт KA7fiJKVJko+dkariOuntA"))
		defer func() {
			// ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Финиш KA7fiJKVJko+dkariOuntA"))
			if err := recover(); err != nil {
				ll.Log(ee.Error().WithMessage(fmt.Sprintf("\n\n\n"+logFunc+" PANIC: %#v\n%s\n\n", err, string(debug.Stack()))))
			}
		}()
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("воркера перезапуска для " + key))
		for {
			select {
			case <-dChan:
				ll.Log(ee.Debug().WithPrefix(logPrefix).
					WithMessage("получили команду на выход consumer для " + key))
				return
			case <-mCh:
				ll.Log(ee.Debug().WithPrefix(logPrefix).
					WithMessage("получили команду на перезапуск consumer для " + key))
				o.reNewConsumer(logPrefix, queue, eChan, dChan, queue.QOS, queue.AutoAck, o.ch, o.stopChan)
				return
			}
		}
	}(o.queueName)
	
	for o.cli.Loop() {
		select {
		case <-dChan:
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Получен сигнал завершения консьюмера для очереди", o.queueName))
			// msg, ok := <-o.cns.Deliveries()
			// if ok {
			// 	msg.Nack(true, true)
			// }
			return
		case msg := <-o.cns.Deliveries():
			if msg.Body != nil {
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Из очереди", o.queueName, "получено сообщение:", string(msg.Body)).WithAt(logFile, logFunc))
				mMsg := Message(msg)
				// select {
				// case o.ch <- &mMsg:
				// default:
				// }
				o.ch <- &mMsg
				if o.autoAck {
					ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Параметр AutoAck для очереди", o.queueName, "установлен. Осуществляется Ack.").WithAt(logFile, logFunc))
					
					if err := msg.Ack(true); err != nil {
						eChan <- err.Error()
					}
				}
			}
		case err := <-o.cns.Errors():
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Получена ошибка из CONSUMER::", o.queueName).WithError(err))
			eChan <- err.Error()
			e, ok := err.(*amqp.Error)
			if ok && e.Code == amqp.NotFound {
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Отправляем struct{}{} для перезапуска", o.queueName))
				mCh <- struct{}{}
				return
			} else if ok && e.Code == amqp.FrameError {
				mCh <- struct{}{}
				return
			}
		case err := <-o.cli.Errors():
			ll.Log(ee.Error().WithPrefix(logPrefix).WithMessage("Получена ошибка из CLIENT::", o.queueName).WithError(err))
			eChan <- err.Error()
			e, ok := err.(*amqp.Error)
			if ok && e.Code == amqp.FrameError {
				ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Отправляем struct{}{} для перезапуска", o.queueName))
				mCh <- struct{}{}
				return
			}
		case blocked := <-o.cli.Blocking():
			eChan <- blocked.Reason
			if blocked.Active {
				ll.Log(ee.Info().WithPrefix(logPrefix).WithAt(logFile, logFunc).
					WithMessage(blocked.Reason + " соединение блокировано со стороны сервера. Спим секунду"))
				time.Sleep(time.Second)
			}
		case <-o.stopChan:
			ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Получен сигнал завершения консьюмера для очереди", o.queueName))
			// msg, ok := <-o.cns.Deliveries()
			// if ok {
			// 	msg.Nack(true, true)
			// }
			return
		}
	}
}

// createURL - создает dsn для соединения с сервером rabbitMQ
func createURL(logPrefix ee.LogPrefixType, cfg Config) string {
	url := "amqp://" + cfg.Server.Login
	
	if len(cfg.Server.Password) > 0 {
		url = url + ":" + cfg.Server.Password
	}
	
	url = url + "@" + cfg.Server.Addr
	
	if cfg.Server.Port != 0 {
		url = fmt.Sprintf("%s:%d", url, cfg.Server.Port)
	}
	
	return url
}

// checkRMQ - проверяет соединение с сервером
func checkRMQ(logPrefix ee.LogPrefixType, url string) error {
	logFunc := "checkRMQ"
	
	ll.Log(ee.Debug().WithPrefix(logPrefix).WithMessage("Проверка соединения с сервером rabbitMQ").WithAt(logFile, logFunc))
	
	conn, err := amqp.Dial(url)
	if conn == nil {
		return ErrNoConn
	}
	defer conn.Close()
	return err
}

// createClient - создает клиента
func createClient(logPrefix ee.LogPrefixType, url string) (*cony.Client, error) {
	var try = 1
	err := errors.New("tmp")
	for err != nil {
		err = checkRMQ(logPrefix, url)
		if err != nil {
			ll.Log(ee.Error().WithPrefix(logPrefix).WithError(err).WithMessage("Спим", try, "секунд"))
			time.Sleep(time.Duration(try) * time.Second)
			if try <= 10 {
				try *= 2
			}
		}
	}
	
	cli := cony.NewClient(
		cony.URL(url),
		cony.Backoff(cony.DefaultBackoff),
	)
	
	return cli, nil
}

// checkKeys - проверка уникальности ключей
func checkKeys(logPrefix ee.LogPrefixType, cfg Config) bool {
	m := make(map[string]int)
	
	for k := range cfg.Queues {
		m[k]++
		if m[k] != 1 {
			return false
		}
	}
	
	for k := range cfg.Exchanges {
		m[k]++
		if m[k] != 1 {
			return false
		}
	}
	
	return true
}

func (o *Obj) ThoroughlySendWithExpiration(logPrefix ee.LogPrefixType, key string, body []byte, headers amqp.Table, duration time.Duration, expiration int64) {
	a := make(map[string]interface{})
	a["expiration"] = strconv.FormatInt(expiration, 10)
	o.ThoroughlySend(logPrefix, key, body, headers, duration, a)
	
}

// ThoroughlySend - безусловна отправка в RMQ сообщения
func (o *Obj) ThoroughlySend(logPrefix ee.LogPrefixType, key string, body []byte, headers amqp.Table, duration time.Duration, additional ...map[string]interface{}) {
	var try = 1
	
	tr := throttling.NewThrottling(o.doneChan).SetDelay(duration)
	
	err := errors.New("tmp")
	for err != nil {
		if err = o.SendMsg(logPrefix, key, "application/json", body, headers, additional...); err != nil {
			ll.Log(ee.Error().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySend").WithError(err).
				WithMessage("не удалось отправить в " + key))
			<-tr.Throttling()
			try++
		}
	}
	if ll.GetLogLevel() == ee.DEBUG {
		kind, to := o.getDstName(key)
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySend").
			WithMessage("успешно отправили в " + kind + " " + to + " за " + strconv.Itoa(try) + " попыт-ку/ок"))
	}
	
}

// ThoroughlySendWithProgression - безусловна отправка в RMQ сообщения
func (o *Obj) ThoroughlySendWithProgression(logPrefix ee.LogPrefixType, key string, body []byte, headers amqp.Table, f func(throttling.Throttling) time.Duration, additional ...map[string]interface{}) {
	var try = 1
	
	tr := throttling.NewThrottling(o.doneChan).SetDelay(time.Second).SetDelayFunc(f)
	
	err := errors.New("tmp")
	for err != nil {
		if err = o.SendMsg(logPrefix, key, "application/json", body, headers, additional...); err != nil {
			ll.Log(ee.Error().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendWithProgression").WithError(err).
				WithMessage("не удалось отправить в " + key))
			<-tr.Throttling()
			try++
		}
	}
	
	if ll.GetLogLevel() == ee.DEBUG {
		kind, to := o.getDstName(key)
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendWithProgression").
			WithMessage("успешно отправили в " + kind + " " + to + " за " + strconv.Itoa(try) + " попыт-ку/ок"))
	}
	
}

// ThoroughlySendPublishing - безусловна отправка в RMQ сообщения
func (o *Obj) ThoroughlySendPublishing(logPrefix ee.LogPrefixType, key string, obj amqp.Publishing, duration time.Duration) {
	var try = 1
	
	tr := throttling.NewThrottling(o.doneChan).SetDelay(duration)
	
	err := errors.New("tmp")
	for err != nil {
		if err = o.Send(logPrefix, key, obj); err != nil {
			ll.Log(ee.Error().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendPublishing").WithError(err).
				WithMessage("не удалось отправить в " + key))
			<-tr.Throttling()
			try++
		}
	}
	
	if ll.GetLogLevel() == ee.DEBUG {
		kind, to := o.getDstName(key)
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendPublishing").
			WithMessage("успешно отправили в " + kind + " " + to + " за " + strconv.Itoa(try) + " попыт-ку/ок"))
	}
}

// ThoroughlySendPublishingWithProgression - безусловна отправка в RMQ сообщения
func (o *Obj) ThoroughlySendPublishingWithProgression(logPrefix ee.LogPrefixType, key string, obj amqp.Publishing, f func(throttling.Throttling) time.Duration) {
	var try = 1
	
	tr := throttling.NewThrottling(o.doneChan).SetDelay(time.Second).SetDelayFunc(f)
	
	err := errors.New("tmp")
	for err != nil {
		if err = o.Send(logPrefix, key, obj); err != nil {
			ll.Log(ee.Error().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendPublishingWithProgression").WithError(err).
				WithMessage("не удалось отправить в " + key))
			<-tr.Throttling()
			try++
		}
	}
	
	if ll.GetLogLevel() == ee.DEBUG {
		kind, to := o.getDstName(key)
		
		ll.Log(ee.Debug().WithPrefix(logPrefix).WithAt("rmq3.go", "ThoroughlySendPublishingWithProgression").
			WithMessage("успешно отправили в " + kind + " " + to + " за " + strconv.Itoa(try) + " попыт-ку/ок"))
	}
}

func (msg Message) GetHeaders() amqp.Table {
	h := make(amqp.Table)
	if msg.Headers == nil {
		return h
	}
	for hk, hv := range msg.Headers {
		h[hk] = hv
	}
	return h
}

func (o *Obj) getDstName(key string) (string, string) {
	kind := "queue"
	to := ""
	
	_, okExchange := o.exchanges[key]
	if okExchange {
		kind = "exchange"
		to = o.exchanges[key].Name
	}
	
	_, okQueue := o.queues[key]
	if okQueue {
		to = o.queues[key].Name
	}
	
	return kind, to
}
